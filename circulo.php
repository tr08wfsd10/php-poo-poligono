<?php
/**
 * 
 */
class Circulo extends Poligono
{	
	private $radio;

	
	public function __construct($radio)
	{
		$this->radio=$radio;
	}

	public function getArea():float
	{
		return $radio*pi();
	}
}


?>